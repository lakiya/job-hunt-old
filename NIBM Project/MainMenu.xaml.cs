﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Windows.Threading;

namespace NIBM_Project
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        private HandleDatabase handleDatabase;
        private bool isOldUser;

        public MainMenu()
        {
            InitializeComponent();
            handleDatabase = new HandleDatabase();
            checkUser();
            try
            {
                handleDatabase.getCompanyData();
                handleDatabase.getVacancyData();
                welcome.Content = StaticJobSeekerID.getName().Split()[0] + "!";
                State.Content = "Ready";
            }
            catch (WebException ex)
            {
                throw ex;
            }
            catch (IndexOutOfRangeException ex)
            {
                throw ex;
            }
            catch (FormatException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            JobSeekerWindow jobseeker = new JobSeekerWindow();
            checkUser();

            if (isOldUser)   
            {               
                jobseeker.Show();
                jobseeker.setOldUser(isOldUser);                              
            }
            jobseeker.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                JobRecommendation jobRecommendation = new JobRecommendation();
                jobRecommendation.Show();
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Cannot Find your current Profile Data. Make Sure You have Completed you Profile.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);              
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Error Occured. Please try to Again.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);              
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            CompanyDetailsForJobSeeker companydetailsWindow = new CompanyDetailsForJobSeeker();
            companydetailsWindow.Show();
        }

        private void checkUser()
        {
            handleDatabase = new HandleDatabase();
            try
            {
                Rest r = new Rest("Users/CheckProfile/" + StaticJobSeekerID.getID());

                if (r.Execute().Equals("true"))
                    isOldUser = true;
                else
                    isOldUser = false;

                if (isOldUser)
                {
                    handleDatabase.getJobSeekerData(StaticJobSeekerID.getID());
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Cannot connect to the Job Hunt Server. Please Check your Internet Connection and try again", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Index Range is out of Bound. Press Ok to Exit", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Data Conversion Failed. Press Ok to Exit", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Error Occured. Please try to Again.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Deleting your profile will wipe out all your profile data from our databases. Do you want to proceed?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                try
                {
                    Rest rest = new Rest("Jobseeker/Delete/" + StaticJobSeekerID.getID());
                    rest.SetMethod("DELETE");
                    rest.Execute();

                    for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                    {
                        MainWindow main = new MainWindow();
                        main.Show();
                        App.Current.Windows[intCounter].Close();
                    }
                }
                catch (WebException ex)
                {
                    Console.WriteLine(ex.ToString());
                    MessageBox.Show("Cannot connect to the Job Hunt Server. Please Check your Internet Connection and try again", "Error", MessageBoxButton.OK, MessageBoxImage.Error);                   
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    MessageBox.Show("Error Occured. Please try to Again.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
            }
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to close all the Windows and Logout? All your unsaved data will be lost", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                {
                    MainWindow main = new MainWindow();
                    main.Show();
                    App.Current.Windows[intCounter].Close();
                }
            }
        }

        private void nothing_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                handleDatabase = new HandleDatabase();
                State.Content = "Busy...";
                State.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate() { State.UpdateLayout(); }));
                using (new WaitCursor())
                {
                    checkUser();
                    handleDatabase.getCompanyData();
                    handleDatabase.getVacancyData();
                }
                State.Content = "Ready";
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Cannot connect to the Job Hunt Server. Please Check your Internet Connection and try again", "Error", MessageBoxButton.OK, MessageBoxImage.Error);            
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Index Range is out of Bound. Press Ok to Exit", "Error", MessageBoxButton.OK, MessageBoxImage.Error);            
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Data Conversion Failed. Press Ok to Exit", "Error", MessageBoxButton.OK, MessageBoxImage.Error);               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("Error Occured. Please try to Again.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);                
            }
        }
    }
}
