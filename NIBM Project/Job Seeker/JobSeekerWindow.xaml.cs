﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace NIBM_Project
{
    /// <summary>
    /// Interaction logic for JobSeekerWindow.xaml
    /// </summary>
    public partial class JobSeekerWindow : Window, INotifyPropertyChanged 
    {
        //Accessors to get Data from TextFields 
        public event PropertyChangedEventHandler PropertyChanged;

        public String firstName { get; set; }
        public String lastName { get; set; }
        public String addressLine1 { get; set; }
        public String addressLine2 { get; set; }
        public String addressLine3 { get; set; }
        public String telMobile { get; set; }
        public String telHome { get; set; }
        public String email { get; set; }
        public DateTime dateOfBirth { get; set; }
        public String title { get; set; }
        public String qualifiedField { get; set; }
        public String interestedPosition { get; set; }
        public String lastJob { get; set; }
        
        public int ID;
        public bool oldUser = false;

        Validate val; //Validate Class Reference
        JobSeeker seeker; //Job Seeker Class Reference
        JobSeekerPreferences seekerPreferences; //Job Seeker Preference Reference
        JobSeekerQualificationWindow forward; //Job Seeker Qualification Window Reference /*to forward objects /*

        public JobSeekerWindow()
        {
            InitializeComponent();
            DataContext = this;
        }
        public void setOldUser(bool oldUser)
        {
            Console.Write("Old User");
            this.oldUser = oldUser;

            this.seeker = StaticJobSeekerData.getJobSeekerPersonalData();           
            String[] name = seeker.getName().Split();
            String[] address = seeker.getAddress().Split();
            FirstName.Text = name[0];           
            LastName.Text = name[1];
            TitleBox.SelectedValue = seeker.getTitle();
            HomeTelephone.Text = "0" + seeker.getTelephoneHome().ToString();
            MobileTelephone.Text = "0" + seeker.getTelephoneMobile().ToString();
            AddressLine1.Text = address[0];
            AddressLine2.Text = address[1];
            AddressLine3.Text = address[2];
            Email.Text = seeker.getEmail();
            DataOfBirth.SelectedDate = seeker.getBirthday();

            this.seekerPreferences = StaticJobSeekerData.getJobSeekerPreferences();
       
            QualifiedField.SelectedValue = seekerPreferences.getQualifiedField();
            InterestedJob.SelectedValue = seekerPreferences.getinterestedPosition();
            LastJob.SelectedValue = seekerPreferences.getLastJob();           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            val = new Validate();

            //Validation

            //Check for Null Fields
            if (FirstName.Text.Length == 0)
            {
                MessageBox.Show("First Name should not be Empty", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (LastName.Text.Length==0)
            {
                MessageBox.Show("Last Name should not be Empty", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (AddressLine1.Text.Length == 0 || AddressLine2.Text.Length == 0)
            {
                MessageBox.Show("Address should not be Empty", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (HomeTelephone.Text.Length==0 || MobileTelephone.Text.Length==0)
            {
                MessageBox.Show("Telephone Number should not be Empty", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (Email.Text.Length==0)
            {
                MessageBox.Show("Email Address should not be Empty", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (Email.Text.Contains("@") == false)
            {
                MessageBox.Show("Emal Address you entered is invalid", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            } 
            else if (TitleBox.SelectedIndex == 0)
            {
                MessageBox.Show("Title Should be Selected", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            else if (QualifiedField.SelectedIndex == 0)
            {
                MessageBox.Show("Qualified field Should be Selected", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            else if (InterestedJob.SelectedIndex == 0)
            {
                MessageBox.Show("Interested job field must be selected", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            //Check Numeric Fields
            else if (!val.isNumeric(MobileTelephone.Text) || !val.isNumeric(HomeTelephone.Text))
            {
                MessageBox.Show("Telephone Number should be Numeric", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            //Check Field Lengths
            else if (!val.isTooShort(HomeTelephone.Text) || !val.isTooShort(MobileTelephone.Text))
            {
                MessageBox.Show("Telephone Number should not be less than 10 Characters", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            else
            {
                seeker = new JobSeeker();
                seekerPreferences = new JobSeekerPreferences();
                ID = StaticJobSeekerID.getID(); //Get the relevent ID from Static variable in StaticJobSeekerID class

                //Set Seeker Personal Info
                seeker.setId(ID);
                seeker.setFullName(firstName, lastName);
                seeker.setTitle(title);
                seeker.setDateOfBirth(dateOfBirth);
                seeker.setTelHome(Convert.ToInt32(telHome));
                seeker.setTelMobile(Convert.ToInt32(telMobile));
                seeker.setAddress(addressLine1, addressLine2, addressLine3);
                seeker.setEmail(email);

                //Set Seeker Preferences
                seekerPreferences.setId(ID);
                seekerPreferences.setInterestedPosition(interestedPosition);
                seekerPreferences.setQualifiedField(qualifiedField);
                seekerPreferences.setLastJob(lastJob);

                forward = new JobSeekerQualificationWindow();
                forward.recieveJobSeekerData(seeker, seekerPreferences, ID);

                if (oldUser)
                {                   
                    forward.Show();
                    this.Close();
                    forward.setOldUser(oldUser);
                }

                this.Close();
                forward.Show();

            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to clear everything? All unsaved data will be lost", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                foreach (UIElement element in jobSeekerPersonalInfoWindowGrid.Children)
                {
                    TextBox textbox = element as TextBox;
                    if (textbox != null)
                    {
                        textbox.Text = String.Empty;
                    }
                }
            }
        }

    }
}
