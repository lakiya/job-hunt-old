﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;

namespace NIBM_Project
{
    /// <summary>
    /// Interaction logic for JobSeekerSignupWindow.xaml
    /// </summary>
    public partial class JobSeekerSignupWindow : Window
    {
        public int id;
        public String name { get; set; }
        public String userName { get; set; }

        Validate validate;

        public JobSeekerSignupWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {   
            validate = new Validate();
            String pw = password.Password.ToString();
            String confirmPw = ConfirmPassword.Password.ToString();

            if(validate.isEmpty(name))
            {
                MessageBox.Show("Name should not be Empty");
            }

            else if(validate.isEmpty(userName))
            {
                MessageBox.Show("Username should not be Empty");
            }

            else if (validate.isEmpty(pw))
            {
                MessageBox.Show("Password should not be Empty");
            }

            else if (password.Password.Length < 5)
            {
                MessageBox.Show("Password should be more than 6 characters", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            else if (!pw.Equals(confirmPw))
            {
                MessageBox.Show("Passwords do not Match");
            }

            else
            {
                try
                {
                    Rest rest = new Rest("Users/Post");
                    rest.SetMethod("POST");

                    Dictionary<string, string> postData = new Dictionary<string, string>();

                    postData.Add("fName", name);
                    postData.Add("username", userName);
                    postData.Add("type", "JobSeeker");
                    postData.Add("password", pw);

                    rest.setFormData(postData);
                    rest.Execute();
                    /*
                    //Temporary Local Database Test
                    HandleDatabase handleDatabase = new HandleDatabase();
                    id = new GetLastID().getID();
                    handleDatabase.execute("INSERT INTO signup (signin_ID, name, user_name,password,type) values (" + id + ",'" + name + "','" + userName + "','" + pw + "','JobSeeker')");
                    */
                    MessageBox.Show("Profile Successfully Created");
                    //Temporary Local Database Test

                    MainWindow mainwindow = new MainWindow();
                    mainwindow.Show();
                    this.Close();
                }
                catch (WebException ex)
                {
                    Console.WriteLine(ex.ToString());
                    MessageBox.Show("Cannot connect to the Job Hunt Server. Please Check your Internet Connection and try again", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                    this.Close();         
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    MessageBox.Show("Error Occured. Please try to Again.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                    this.Close();          
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to close the window? All your unsaved data will be lost", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                MainWindow main = new MainWindow();
                main.Show();
                this.Close();
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to clear everything? All unsaved data will be lost", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                foreach (UIElement element in jobSeekerSignupGrid.Children)
                {
                    TextBox textbox = element as TextBox;
                    if (textbox != null)
                    {
                        textbox.Text = String.Empty;
                    }
                }
            }
        }
    }
}
